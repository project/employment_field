
Employment Field is a CCK field for storing information about a person's employment history.

It contains fields for company, title, office, division or section, address, description, and duration.

Its usefulness is mainly when used as a multiple field, allowing users to submit multiple entries for the various jobs they have held.  It uses javascript (based on link.module) to allow the user to add additional employment fields to the node form as needed.

See also the analogous module Education Field: http://drupal.org/project/education_field